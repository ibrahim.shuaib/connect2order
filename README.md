# Connect2order

## INTRODUCTION
Connect2order is a web platform for local businesses to connect with customers that offers online ordering, delivery or booking services. 

## FUNCTIONS

### Business Account
-Businesses can create accounts with basic information (name, contact details, location, description, etc).
-Businesses can upload logos, photos, and videos to showcase their offerings.
-Businesses can manage their menus, service listings, or appointment slots.
-Businesses can set pricing and availability for their offerings.

### Customer Functionality
-Customers can browse businesses by category, location, or keywords. -Customers can view detailed business profiles, menus, service descriptions, or appointment schedules. -Customers can book appointments for haircuts, massages, or fitness classes.
-Customers can leave their reviews and ratings for businesses.

### Admin Panel
-Admin can manage user accounts (businesses and customers).
-Admin can monitor platform activity (orders, bookings, reviews).
-Admin can generate reports and analytics for businesses.
-Admin can manage platform settings and configurations.

## TECHNICAL SPECIFICATIONS
### Backend
Django framework for server-side development

### Database
MySQL for storing user, business, and service data.

## Installation
Connect2order is primarily a web application.

## Contributing
The users that contributed to this development are; ibrahim.shuaib, Nabagereka002, and Yokas

## Authors and acknowledgment
We appreciate and acknowledge Mr. Ibrahim.S.Sempijja, Miss. Nabagereka Bridget, and Mr. Yokoyasi Gerald for directly contributing towards this project.