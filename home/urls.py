from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('connect/', views.connect, name="connect"),
    path('cart/', views.cart, name="cart"),
    path('checkout/', views.checkout, name="checkout"),
    path('business/', views.business, name="business"),
    path('loginUser/', views.loginUser, name="loginUser"),
    path('registerUser/', views.registerUser, name="registerUser"),
    path('product/<int:product_id>/', views.product_details, name='product_details'),

    path('update_item/', views.updateItem, name="update_item"),
    path('process_order/', views.processOrder, name="process_order"),
]